const env           = require('dotenv').config();

exports.successResponse = function(results, msg){
    return {
        success: true,
        msg: msg,
        results: results
    }
};

exports.errorResponse = function(err){
    return {
        success: false,
        results: err
    }
};