var FuncHelpers     = require('../../../helpers/FuncHelpers');
var Exchange        = require('../../../models/api/v1/exchange');
var Moment          = require('moment');

exports.getExchange = function(req, res, next){
    Exchange.find().exec()
            .then((exc)=>{
                res.status(200).json(FuncHelpers.successResponse(exc));
            })
            .catch((err)=>{
                res.status(422).json(FuncHelpers.errorResponse(err));
            });
}

exports.insertExchange = function(req, res, next){
    Exchange.create(req.body)
        .then((exc) => {
            res.status(201).json(FuncHelpers.successResponse(exc))
        })
        .catch((err) => {
            res.status(422).json(FuncHelpers.errorResponse(err))
        })
}

exports.deleteExchange = function(req, res) {
    let id          = req.params.id; 
    Exchange.findByIdAndRemove(id).exec()
        .then((exc)=>{
            res.status(200).json(FuncHelpers.successResponse(exc));
        })
        .catch((err)=>{
            res.status(422).json(FuncHelpers.errorResponse(err));
        });
}

exports.updateExchange = function(req, res, next){  
    let id          = req.params.id; 
    let data_update   = req.body;
    Exchange.findOneAndUpdate({"_id":id}, data_update).exec()
        .then((exc)=>{
            res.status(200).json(FuncHelpers.successResponse(data_update));
        })
        .catch((err)=>{
            res.status(422).json(FuncHelpers.errorResponse(err));
        });
}

exports.removeExchange = function(req, res) {
    Exchange.remove({ $and:[ {'from':req.params.from}, {'to':req.params.to} ] })
        .then((exc)=>{
            res.status(200).json(FuncHelpers.successResponse(exc));
        })
        .catch((err)=>{
            res.status(422).json(FuncHelpers.errorResponse(err));
        });
}

exports.getExchangeWithFromTo = function(req, res, next){
    Exchange.find({ $and:[ {'from':req.params.from}, {'to':req.params.to} ] }).exec()
            .then((exc)=>{
                res.status(200).json(FuncHelpers.successResponse(exc));
            })
            .catch((err)=>{
                res.status(422).json(FuncHelpers.errorResponse(err));
            });
}

exports.getExchangeWithFromToAvg = async (req, res, next)=>{

    try {
        var average = await Exchange.aggregate([
                        {$match: { 
                                from: req.params.from, 
                                to : req.params.to 
                            } 
                        },
                        {$group: {
                                _id: {
                                    from : req.params.from, 
                                    to : req.params.to
                                }, 
                                average: {$avg: '$rate'}
                            }
                        }
                    ])
                    .sort({ date : 1 })
                    .limit(7)
                    .exec();
            average = average[0].average;

        var max = await Exchange.aggregate([
                    {$match: { 
                            from: req.params.from, 
                            to : req.params.to 
                        } 
                    },
                    {$group: {
                            _id: {
                                from : req.params.from, 
                                to : req.params.to
                            }, 
                            max: {$max: '$rate'}
                        }
                    }
                ])
                .sort({ date : 1 })
                .limit(7)
                .exec();
            max = max[0].max;

        var min = await Exchange.aggregate([
                    {$match: { 
                            from: req.params.from, 
                            to : req.params.to 
                        } 
                    },
                    {$group: {
                            _id: {
                                from : req.params.from, 
                                to : req.params.to
                            }, 
                            min: {$min: '$rate'}
                        }
                    }
                ])
                .sort({ date : 1 })
                .limit(7)
                .exec();
            min = min[0].min;

        let variance = max-min;

        var detail = await Exchange.find(
                        { $and:[ 
                            {'from':req.params.from}, 
                            {'to':req.params.to} 
                        ] })
                        .sort({ date : 1 })
                        .limit(7)
                        .exec();

        let result = {
                average :average,
                Variance: variance,
                detail: detail
            };

        res.status(200).json(FuncHelpers.successResponse(result));
    } catch (err) {
        res.status(422).json(FuncHelpers.errorResponse(err));
    }
}

exports.getExchangeWithDate = async (req, res, next)=>{
    var date_until  = new Date(req.body.date);
    var date_from   = new Date(req.body.date);
    date_from.setDate(date_from.getDate()-7);
    date_from = date_from.getFullYear()+'-'+(Number(date_from.getMonth())+1)+'-'+date_from.getDate();
    
    try {
        var data = await Exchange.aggregate([
                    {$match: { 
                            date: {
                                "$gte": new Date(date_from),
                                "$lte": date_until
                            }
                        } 
                    },
                    {$group: {
                            _id: {
                                from : "$from", 
                                to : "$to"
                            },
                            average: {$avg: '$rate'}
                        }
                    }
                ])
                .exec();

        var length = data.length
        var result = []

        for (var i = 0; i < length; i++) {

            var perdate = await Exchange.find(
                {
                    "date": {
                        "$gte": date_until,
                        "$lte": date_until
                    },
                    "from" : data[i]._id.from, 
                    "to" : data[i]._id.to
                }, 'rate')
                .exec()
            perdate = perdate[0].rate  

            var values = {
                from: data[i]._id.from,
                to: data[i]._id.to,
                rate: perdate,
                average: data[i].average
            }

            result.push(values)
        }

        res.status(200).json(FuncHelpers.successResponse(result));
    } catch (err) {
        res.status(422).json(FuncHelpers.errorResponse(err));
    }
}






