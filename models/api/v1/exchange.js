const mongoose      = require("mongoose");
const Schema        = mongoose.Schema;
var validate        = require('mongoose-validator');

const exchangeSchema = new Schema({
    from: {
        type: String,
        required: true,
        uppercase: true,
        maxlength: 3
    },
    to: {
        type: String,
        required: true,
        uppercase: true,
        maxlength: 3
    },
    rate: {
        type: Number,
        required: true
    },
    date: { type: Date, default: Date.now },
}, { collection: 'exchage' });

var Exchange = mongoose.model("Exchage", exchangeSchema);
module.exports = Exchange;