var express = require('express');
var router = express.Router();
var exchageController = require('../../../controllers/api/v1/exchageController.js');

router.get('/', exchageController.getExchange)
    .post('/', exchageController.insertExchange);
router.put('/:id', exchageController.updateExchange)
    .delete('/:id', exchageController.deleteExchange);
router.get('/:from/:to', exchageController.getExchangeWithFromTo);
router.get('/ecx_avg/:from/:to', exchageController.getExchangeWithFromToAvg);
router.get('/ecx_date', exchageController.getExchangeWithDate);
router.delete('/remove-exchange/:from/:to', exchageController.removeExchange);

module.exports = router;
