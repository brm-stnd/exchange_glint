var express = require('express');
var router = express.Router();
const exchangeRouter = require('./exchange');

router.use('/exchange', exchangeRouter);

router.get('/', function (req, res, next) {
  res.render('index', { title: 'API V1' });
});

module.exports = router;
